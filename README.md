This project is a very simple project based on Symfony 5 for evaluate Symfony learning

### Run the docker
In the root of the directory run below commands:
```sh
$ docker-compose up -d --build

$ docker-compose exec app composer install
$ docker-compose exec app php bin/console doc:mig:mig
$ docker-compose exec app php bin/console doctrine:fixtures:load
```

## Run the App

- Open your browser and :
    - visit application : **http://127.0.0.1:8080**
    - visit phpMyAdmin  : **http://127.0.0.1:8585**

**Best regards**,

Mojtaba Gheytasi