<?php

namespace App\Service\Basket;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\Storage\Contract\StorageInterface;

class BasketService
{
    private StorageInterface $storage;

    private ProductRepository $productRepository;

    public function __construct(StorageInterface $storage, ProductRepository $productRepository)
    {
        $this->storage = $storage;
        $this->productRepository = $productRepository;
    }

    public function add(Product $product, int $quantity)
    {
        if ($this->has($product)) {
            $quantity = $this->get($product)['quantity'] + $quantity;
        }

        $this->update($product, $quantity);
    }

    public function itemCount(): int
    {
        return $this->storage->count();
    }

    public function all(): array
    {
        $this->storage->all();

        $products = $this->productRepository->findBy([
            'id' => array_keys($this->storage->all())
        ]);

        foreach ($products as $product) {
            $product->setQuantity($this->get($product)['quantity']);
        }

        return $products;
    }

    public function subTotal(): int
    {
        return array_reduce($this->all(), function ($total, $item) {

            $total += $item->getPrice() * $item->getQuantity();

            return $total;

        }, 0);
    }

    public function update(Product $product, int $quantity)
    {
        if (! $product->hasStock($quantity)) {
            throw new QuantityExceededException;
        }

        $this->storage->set($product->getId(), [
            'quantity' => $quantity
        ]);
    }

    public function delete(Product $product)
    {
        $this->storage->unset($product->getId());
    }

    private function has(Product $product): bool
    {
        return $this->storage->exists($product->getId());
    }

    private function get(Product $product)
    {
        return $this->storage->get($product->getId());
    }

    public function clear()
    {
        return $this->storage->clear();
    }
}