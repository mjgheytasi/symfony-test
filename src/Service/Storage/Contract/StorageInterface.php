<?php

namespace App\Service\Storage\Contract;

interface StorageInterface
{
    public function get(string $key);

    public function set(string $key, $value);

    public function all(): array;

    public function exists(string $key);

    public function unset(string $key);

    public function clear();
}