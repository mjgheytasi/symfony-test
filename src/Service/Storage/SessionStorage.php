<?php

namespace App\Service\Storage;

use App\Service\Storage\Contract\StorageInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionStorage implements StorageInterface,\Countable
{
    private string $bucket;

    private SessionInterface $session;

    public function __construct(string $bucket, SessionInterface $session)
    {
        $this->bucket = $bucket;
        $this->session = $session;
    }

    public function get(string $key)
    {
        return $this->session->get($this->bucket)[$key];
    }

    public function set(string $key, $value): void
    {
        $this->session->set(
            $this->bucket,
            [$key => $value] + $this->all()
        );
    }

    public function all(): array
    {
        return $this->session->get($this->bucket) ?? [];
    }

    public function exists(string $key): bool
    {
        return isset($this->all()[$key]);
    }

    public function unset(string $key): void
    {
        $allItems = $this->all();

        unset($allItems[$key]);

        $this->session->set($this->bucket, $allItems);
    }

    public function clear(): void
    {
        $this->session->remove($this->bucket);
    }

    public function count(): int
    {
        return count($this->all());
    }

}