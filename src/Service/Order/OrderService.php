<?php

namespace App\Service\Order;

use App\Entity\Enum\OrderStatusEnum;
use App\Entity\Enum\PaymentMethodEnum;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\User;
use App\Service\Basket\BasketService;
use App\Utils\UniqueNumberGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class OrderService
{
    private BasketService $basketService;

    private EntityManagerInterface $entityManager;

    private Order $order;

    public function __construct(
        BasketService $basketService,
        EntityManagerInterface $entityManager
    ) {
        $this->basketService = $basketService;
        $this->entityManager = $entityManager;
    }

    public function create(UserInterface $user, Request $request): Order
    {
        try {
            $this->entityManager->beginTransaction();

            $this->createOrder($user, $request->request->get('paymentMethod'));

            $this->createOrderItem();

            $this->entityManager->flush();

            $this->normalizeProductQuantity();

            $this->entityManager->commit();

            return $this->order;

        } catch (\Exception $exception) {

            $this->entityManager->commit();

            throw new \Exception();
        }
    }

    private function createOrderItem()
    {
        foreach ($this->basketService->all() as $basketProduct) {
            $orderItem = new OrderItem;
            $orderItem->setQuantity($basketProduct->getQuantity())
                ->setProduct($basketProduct)
                ->setOrderRelation($this->order)
                ->setAmount($basketProduct->getPrice());

            $this->order->addOrderItem($orderItem);

            $this->entityManager->persist($orderItem);
        }
    }

    private function createOrder(UserInterface $user, string $paymentMethod)
    {
        $this->order = new Order;
        $this->order->setCode(UniqueNumberGenerator::make())
            ->setStatus(OrderStatusEnum::UNPAID)
            ->setOwner($user)
            ->setPaymentType(PaymentMethodEnum::get($paymentMethod))
            ->setTotalAmount($this->basketService->subTotal());

        $this->entityManager->persist($this->order);
    }

    private function normalizeProductQuantity()
    {
        foreach ($this->order->getOrderItems() as $orderItem) {

            $orderItem->getProduct()->decrementStock($orderItem->getQuantity());
        }

        $this->entityManager->flush();
    }

}