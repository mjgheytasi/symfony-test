<?php

namespace App\Factory;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @method static Product|Proxy createOne(array $attributes = [])
 * @method static Product[]|Proxy[] createMany(int $number, $attributes = [])
 * @method static Product|Proxy find($criteria)
 * @method static Product|Proxy findOrCreate(array $attributes)
 * @method static Product|Proxy first(string $sortedField = 'id')
 * @method static Product|Proxy last(string $sortedField = 'id')
 * @method static Product|Proxy random(array $attributes = [])
 * @method static Product|Proxy randomOrCreate(array $attributes = [])
 * @method static Product[]|Proxy[] all()
 * @method static Product[]|Proxy[] findBy(array $attributes)
 * @method static Product[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Product[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ProductRepository|RepositoryProxy repository()
 * @method Product|Proxy create($attributes = [])
 */
final class ProductFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function getDefaults(): array
    {
        return [
            'title' => self::getTitles()[array_rand(self::getTitles())],
            'stock' => random_int(1, 50),
            'price' => random_int(10, 50),
            'description' => self::faker()->paragraph(),
            'views' => random_int(100, 5000),
            'imageName' => 'p' . random_int(1,6) . '.jpg',
        ];
    }

    protected function initialize(): self
    {
        return $this;
    }

    protected static function getClass(): string
    {
        return Product::class;
    }

    private static function getTitles(): array
    {
        return [
            'Classic T-Shirt Dummy',
            'Hoodies Fashion Athletic',
            'Dummy T-Shirt Cool',
            'Casual Tunics Cool',
            'Polo Shirt Cool Quick-Dry',
            'Sleeve Tunic Dummy',
            'Sweater Knitted Cool',
            'Dress Long Sleeve Cool',
            'Midi Sheath Dresses',
            'Cool Quick-Dry Dummy',
        ];
    }
}
