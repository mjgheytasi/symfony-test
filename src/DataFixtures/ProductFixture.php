<?php

namespace App\DataFixtures;

use App\Factory\CategoryFactory;
use App\Factory\ProductFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixture extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        ProductFactory::new()
            ->many(45)
            ->create(function () {
                return ['category' => CategoryFactory::random()];
            });
    }
    public function getDependencies(): array
    {
        return [
            CategoryFixture::class,
        ];
    }
}
