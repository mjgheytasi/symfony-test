<?php

namespace App\DataFixtures;

use App\Factory\CategoryFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        foreach ($this->getData() as $title) {

            CategoryFactory::new(['title' => $title,])->create();
        }
    }

    private function getData(): array
    {
        return [
            'sportswear',
            'fashion',
            'clothing',
            'shoes',
            'households',
        ];
    }
}
