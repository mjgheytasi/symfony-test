<?php

namespace App\Entity\Enum;

class PaymentMethodEnum extends Enum
{
    public const ONLINE = 1;
    public const COD = 2;
}