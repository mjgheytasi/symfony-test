<?php

namespace App\Entity\Enum;

class OrderStatusEnum extends Enum
{
    public const UNPAID = 1;
    public const PAID = 2;
}