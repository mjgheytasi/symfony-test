<?php

declare(strict_types=1);

namespace App\Entity\Enum;

abstract class Enum
{
    private static ?array $constCacheArray = null;

    private function __construct()
    {
        # Preventing Instance
    }

    private static function getConstants(): array
    {
        if (self::$constCacheArray === null) {
            self::$constCacheArray = [];
        }

        $calledClass = get_called_class();

        if (! array_key_exists($calledClass, self::$constCacheArray)) {

            $reflect = new \ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }

        return self::$constCacheArray[$calledClass];
    }

    public static function getKeys(): array
    {
        return array_keys(self::getConstants());
    }

    public static function getKey(int $value): string
    {
        $constants = self::getConstants();

        if (! in_array($value, $constants)) {
            self::createInvalidArgumentException('key');
        }

        return array_search($value, $constants);
    }

    public static function get(string $key): int
    {
        $constants = self::getConstants();

        if (! array_key_exists($key, $constants)) {
            self::createInvalidArgumentException('value');
        }

        return $constants[$key];
    }

    private static function createInvalidArgumentException(
        string $target
    ): \InvalidArgumentException {

        throw new \InvalidArgumentException(
            sprintf('Invalid argument, %s not exists in %s', $target, static::class)
        );
    }

}