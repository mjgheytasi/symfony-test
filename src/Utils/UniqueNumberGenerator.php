<?php

declare(strict_types=1);

namespace App\Utils;

class UniqueNumberGenerator
{
    public static function make(): string
    {
        return time() . rand(10,99);
    }
}