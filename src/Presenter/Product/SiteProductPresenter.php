<?php

namespace App\Presenter\Product;

use App\Entity\Product;
use App\Presenter\Contract\Presenter;

class SiteProductPresenter extends Presenter
{
    public function price(): string
    {
        return sprintf('$ %s', $this->entity->getPrice());
    }

    public function imageUrl(string $uri): string
    {
        return sprintf(
            '%s%s%s',
            $uri,
            Product::IMAGE_URL,
            $this->entity->getImageName()
        );
    }

}