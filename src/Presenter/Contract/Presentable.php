<?php

namespace App\Presenter\Contract;

use App\Presenter\Contract\Exception\EntityPresenterNotFound;

trait Presentable
{
    protected ?Presenter $presenterInstance = null;

    /**
     * @return Presenter
     * @throws EntityPresenterNotFound
     */
    public function getSitePresenter(): Presenter
    {
        if (! $this->sitePresenter || ! class_exists($this->sitePresenter)) {
            throw new EntityPresenterNotFound(
                'sitePresenter property not found in the entity 
                OR adminPanelPresenter class not exists.'
            );
        }

        if ($this->presenterInstance === null) {
            $this->presenterInstance = new $this->sitePresenter($this);
        }

        return $this->presenterInstance;
    }
}