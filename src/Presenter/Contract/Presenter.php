<?php

namespace App\Presenter\Contract;

abstract class Presenter
{
    protected Object $entity;

    public function __construct($entity)
    {
        $this->entity = $entity;
    }

}