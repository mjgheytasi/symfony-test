<?php

namespace App\Presenter\Order;

use App\Entity\Enum\OrderStatusEnum;
use App\Presenter\Contract\Presenter;

class SiteOrderPresenter extends Presenter
{
    public function createdAt(): string
    {
        return $this->entity->getCreatedAt()->format('Y-m-d H:i');
    }

    public function totalAmount(): string
    {
        return sprintf('$ %s', $this->entity->getTotalAmount());
    }

    public function status(): string
    {
        $status = OrderStatusEnum::getKey($this->entity->getStatus());

        $map = [
            OrderStatusEnum::PAID => 'success',
            OrderStatusEnum::UNPAID => 'danger',
        ][$this->entity->getStatus()];

        return "<span class='badge light badge-{$map}'>" . $status . "</span>";
    }
}