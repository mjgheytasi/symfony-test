<?php

namespace App\Controller\Site;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home_index")
     * @param ProductRepository $productRepository
     * @param CategoryRepository $categoryRepository
     *
     * @return Response
     */
    public function index(
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository
    ): Response {

        $mostViewedProducts = $productRepository->findMostViewedProducts(9);
        $latestProducts = $productRepository->findLatestProducts(9);

        return $this->render('site/home/home.html.twig', [
                'mostViewedProducts' => $mostViewedProducts,
                'latestProducts' => $latestProducts,
            ]
        );
    }
}
