<?php

namespace App\Controller\Site;

use App\Entity\Enum\PaymentMethodEnum;
use App\Entity\Product;
use App\Service\Basket\BasketService;
use App\Service\Basket\QuantityExceededException;
use App\Service\Order\OrderService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BasketController extends AbstractController
{
    private BasketService $basketService;
    
    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    /**
     * @Route("/basket", name="basket_index")
     *
     * @return Response
     */
    public function index(): Response
    {
        $basketProducts = $this->basketService->all();
        $subTotal = $this->basketService->subTotal();

        return $this->render('site/basket/index.html.twig', [
            'basketProducts' => $basketProducts,
            'subTotal' => $subTotal,
        ]);
    }

    /**
     * @Route("/basket/add/{product}", name="basket_add")
     *
     * @param Product $product
     *
     * @return Response
     *
     * @IsGranted("ROLE_USER")
     */
    public function add(Product $product): Response
    {
        try {
            $this->basketService->add($product, 1);

            $this->addFlash('success','Product added to your basket successfully');

            return $this->redirectToRoute('basket_index');

        } catch (QuantityExceededException $exception) {

            $this->addFlash('error', 'Product quantity exceeded');

            return $this->redirectToRoute('basket_index');
        }
    }

    /**
     * @Route("/basket/clear", name="basket_clear")
     *
     * @return Response
     *
     * @IsGranted("ROLE_USER")
     */
    public function clear(): Response
    {
        $this->basketService->clear();

        return $this->redirectToRoute('home_index');
    }

    /**
     * @Route("/basket/update/{product}", name="basket_update_item")
     *
     * @param Product $product
     * @param Request $request
     *
     * @return Response
     *
     * @IsGranted("ROLE_USER")
     */
    public function update(Product $product, Request $request): Response
    {
        try {
            $this->basketService->update($product, $request->request->get('quantity', 1));

            return $this->redirectToRoute('basket_index');

        } catch (QuantityExceededException $exception) {

            $this->addFlash('error', 'Product quantity exceeded');

            return $this->redirectToRoute('basket_index');
        }

    }

    /**
     * @Route("/basket/delete/{product}", name="basket_delete_item")
     *
     * @param Product $product
     *
     * @return Response
     */
    public function delete(Product $product): Response
    {
        $this->basketService->delete($product);

        return $this->redirectToRoute('basket_index');
    }

    /**
     * @Route("/checkout", name="checkout_form", methods={"GET"})
     *
     * @return Response
     *
     * @IsGranted("ROLE_USER")
     */
    public function checkoutForm(): Response
    {
        $basketProducts = $this->basketService->all();

        $subTotal = $this->basketService->subTotal();

        return $this->render('site/basket/checkout.html.twig', [
            'basketProducts' => $basketProducts,
            'subTotal' => $subTotal,
        ]);
    }

    /**
     * @Route("/checkout", name="checkout", methods={"POST"})
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param OrderService $orderService
     *
     * @return Response
     *
     * @IsGranted("ROLE_USER")
     */
    public function checkout(
        Request $request,
        ValidatorInterface $validator,
        OrderService $orderService
    ): Response {
        $errors = $this->validateRequest($request, $validator);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->addFlash('error', $error->getMessage());
            }

            return $this->redirectToRoute('checkout_form');
        }

        try {

            $orderService->create($this->getUser(), $request);

            $this->basketService->clear();

//            return $paymentService->doPayment($order, $request->request->get('paymentMethod');

            $this->addFlash('success', 'Your order has been successfully registered');

            return $this->redirectToRoute('home_index');

        } catch (\Exception $exception) {
            // Emergency log
            return $this->redirectToRoute('checkout_form');
        }

    }

    private function validateRequest(
        Request $request,
        ValidatorInterface $validator
    ): ConstraintViolationListInterface {

        $constraints = [
            new Assert\Choice(PaymentMethodEnum::getKeys()),
            new Assert\NotBlank([
                'message' => 'Please select payment method'
            ])
        ];

        return $validator->validate(
            $request->request->get('paymentMethod'),
            $constraints
        );
    }
}
