<?php

namespace App\Controller\Site;

use App\Repository\OrderItemRepository;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private OrderRepository $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @Route("/profile/orders", name="site_profile_order")
     */
    public function getAll(): Response
    {
        $orders = $this->orderRepository->findBy([
            'owner' => $this->getUser()
        ]);

        return $this->render('user-panel/order/order.html.twig', [
            'orders' => $orders
        ]);
    }

    /**
     * @Route("/profile/orders/{code}/items", name="site_profile_order_items")
     */
    public function getOrderItems(
        OrderItemRepository $orderItemRepository,
        Request $request
    ): Response {

        $orderItems = $orderItemRepository->findByOrderCode($request->get('code'));

        return $this->render('user-panel/order/order-item.html.twig', [
            'orderItems' => $orderItems
        ]);
    }

}
