<?php

namespace App\Controller\Site;

use App\Entity\Enum\PaymentMethodEnum;
use App\Entity\Product;
use App\Entity\User;
use App\Service\Basket\BasketService;
use App\Service\Basket\QuantityExceededException;
use App\Service\Order\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PaymentController extends AbstractController
{
    private BasketService $basketService;
    
    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    /**
     * @Route("/payment/verify", name="payment_verify")
     * @param Request $request
     *
     * @return Response
     */
    public function verify(Request $request): Response
    {
        // Validate Verify Request

        $this->addFlash('success','Your order has been successfully registered');

        return $this->redirectToRoute('home_index');
    }
}
