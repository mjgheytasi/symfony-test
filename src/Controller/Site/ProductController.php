<?php

namespace App\Controller\Site;

use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/products",  name="site_products")
     * @param ProductRepository $productRepository
     * @param CategoryRepository $categoryRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     *
     * @return Response
     */
    public function index(
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository,
        Request $request,
        PaginatorInterface $paginator
    ): Response {

        // Validate Data

        $category = $categoryRepository->findOneBy([
            'slug' => $request->get('category')
        ]);

        $productsQuery = $productRepository->findByFilter(
            $request->get('searchTerm'),
            $category,
            $request->get('sortDirection')
        );

        $pagination = $paginator->paginate(
            $productsQuery,
            $request->get('page', 1),
            $request->get('itemPerPage', 6)
        );

        return $this->render('site/product/index.html.twig', [
            'products' => $pagination->getItems(),
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/products/{slug}", name="site_products_show")
     * @param Product $product
     * @param ProductRepository $productRepository
     *
     * @return Response
     */
    public function show(
        Product $product,
        ProductRepository $productRepository
    ): Response {

        $similarProducts = $productRepository->findSimilarProducts($product, 9);

        return $this->render('site/product/details.html.twig', [
            'product' => $product,
            'similarProducts' => $similarProducts,
        ]);
    }
}
