<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findMostViewedProducts(int $numberOfItems)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.views','DESC')
            ->setMaxResults($numberOfItems)
            ->getQuery()
            ->getResult();
    }

    public function findLatestProducts(int $numberOfItems)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.createdAt','DESC')
            ->setMaxResults($numberOfItems)
            ->getQuery()
            ->getResult();
    }

    public function findSimilarProducts(Product $product, int $numberOfItems)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.category = :category')
            ->setParameter('category', $product->getCategory())
            ->andWhere('p.id != :id')
            ->setParameter('id', $product->getId())
            ->orderBy('p.views', 'DESC')
            ->setMaxResults($numberOfItems)
            ->getQuery()
            ->getResult();
    }

    public function findByFilter(?string $searchTerm, ?Category $category, ?string $sortDirection): Query
    {
        $query = $this->createQueryBuilder('p');

        if ($category !== null) {
            $query->andWhere('p.category = :category' )
                ->setParameter('category', $category);
        }

        if ($searchTerm !== null) {
            $query->andWhere('p.title LIKE :searchTerm OR p.description LIKE :searchTerm')
                ->setParameter('searchTerm','%' . $searchTerm . '%');
        }

        if ($sortDirection !== null) {
            $query = $query->orderBy('p.price', $sortDirection);
        }

        return $query->getQuery();
    }

}
