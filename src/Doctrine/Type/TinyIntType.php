<?php

namespace App\Doctrine\Type;

use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class TinyIntType extends Type
{
    public const TINYINT = 'tinyint';

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return "TINYINT";
    }

    public function getName(): string
    {
        return self::TINYINT;
    }

    public function getBindingType(): int
    {
        return ParameterType::INTEGER;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?int
    {
        return $value === null ? null : (int)$value;
    }
}